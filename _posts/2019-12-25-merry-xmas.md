---
layout: post
title: "Merry Christmas from Nanjing"
lang: en
date: 2019-12-25
abstract: "We wish everybody a Christmas holiday of joy and happiness!"
fimage: "/assets/img/snow-of-nanjing.jpg"
fimage_credit: "Dechao Zhang, CC BY-NC-SA 4.0"
ref: merry-xmas
---
We wish everybody a Christmas holiday of joy and happiness!

As the New Year approaches, we are also counting down to our Forum which is going to take place in late September. Can't wait to see you all in Nanjing！

Shortly after the Christmas break, we will announce more details regarding the Forum. But for now, please have a good time with family and friends.
