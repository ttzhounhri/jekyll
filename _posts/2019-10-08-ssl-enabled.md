---
layout: post
title: "SSL encryption enabled"
lang: en
date: 2019-10-08
abstract: "We have encrypted our entire website now to provide extra protection for you."
fimage: "/assets/img/web-security.jpg"
fimage_credit: "Pixabay, license free"
raction: "Know more"
ref: ssl-enabled
---

We value your security.

From now on, our entire site has enforced [SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) encryption. SSL encryption provides an extra layer of protection while you use our website.

{% include figure.html url="/assets/img/how-ssl-works-en-dadian-2018.jpg" caption="A flow chart explaining how SSL works (Dadian, 2019)" %}

Thank you and enjoy!

## Reference
1. Dadian, D. (2019). SSL - what it means, how it works and where it is used. [online] *Powersolution.com*. Available [here](https://www.powersolution.com/ssl-what-it-means-how-it-works-whereused/) [Accessed 8 Oct. 2019].
